package StepDefinition;		

import cucumber.api.java.en.Given;		
import cucumber.api.java.en.Then;		
import cucumber.api.java.en.When;		

public class Scenario1 {				

     
    @Given("^Checking if Given Annotation is working$")				
    public void open_the_browser_and_launch_the_application() throws Throwable							
    {		
        System.out.println("This Step verify Given Annotation is working.");					
    }		

    @When("^Checking if When Annotation is working$")					
    public void enter_the_Username_and_Password() throws Throwable 							
    {		
       System.out.println("This Step verify When Annotation is working.");					
    }		

    @Then("^Checking if Then Annotation is working$")					
    public void Reset_the_credential() throws Throwable 							
    {    		
        System.out.println("This Step verify Then Annotation is working.");					
    }		

}