package StepDefinition;		

import org.openqa.selenium.By;		
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;		
import cucumber.api.java.en.Then;		
import cucumber.api.java.en.When;		

public class Scenario2 {				

    WebDriver driver;		
    
    @Before public void setUp(){ 
        System.setProperty("webdriver.chrome.driver", ".//Resources/driver/chromedriver.exe");					
        driver= new ChromeDriver();					
        driver.manage().window().maximize();	
     } 
    		
    @Given("^Open the Browser and launch the application$")					
    public void open_the_browser_and_launch_the_application() throws Throwable							
    {		
		
       driver.get("http://demo.guru99.com/v4");					
    }		

    @When("^Enter the Username and Password$")					
    public void enter_the_Username_and_Password() throws Throwable 							
    {		
       driver.findElement(By.name("uid")).sendKeys("username12");							
       driver.findElement(By.name("password")).sendKeys("password12");							
    }		

    @Then("^Reset the credential$")					
    public void Reset_the_credential() throws Throwable 							
    {		
       driver.findElement(By.name("btnReset")).click();					
    }		
    
    @After public void cleanUp(){ 
        driver.close(); 
    }
}	